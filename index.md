---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
---
<section class="hero is-medium is-dark is-bold">
  <div class="hero-body">
    <div class="container">
      <h1 class="is-1 title">Por que importa el HTTPS?</h1>
      <p class="subtitle">Siempre debemos preferir los sitios seguros (https://) por sobre los inseguros (http://) aunque no estemos transmitiendo informacion privada (tarjetas de credito, direcciones del hogar/oficina, etc)</p>
      <p class="subtitle">HTTPS no permite que "intrusos" esten "escuchando" tus comunicacione mientras navegas por internet. Cada solicitud de un sitio web usando HTTP puede potencialmente revelar informacion acerca de tu comportamiento como de tus identidades.</p>
    </div>
  </div>
</section>
<section id="gob">
  <div class="container content">
    <h2 class="is-2">Sitios Gubernamentales</h2>
    <table class="table is-striped is-bordered is-fullwidth">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Website</th>
          <th>Twitter</th>
          <th>Facebook</th>
        </tr>
      </thead>
      <tbody>
        {% for s in site.gob %}
        <tr>
          <td>{{s.name}}</td>
          <td><a href="{{s.website}}">link</a></td>
          <td><a href="{{s.rss.twitter.url}}">link</a></td>
          <td><a href="{{s.rss.facebook.url}}">link</a></td>
        </tr>
        {% endfor %}
      </tbody>
    </table>
  </div>
</section>
<section id="press">
  <div class="container content">
    <h2 class="is-2">Prensa</h2>
    <table class="table is-striped is-bordered is-fullwidth">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Website</th>
          <th>Twitter</th>
          <th>Facebook</th>
        </tr>
      </thead>
      <tbody>
        {% for s in site.data.press %}
        <tr>
          <td>{{s.name}}</td>
          <td>{{s.description}}</td>
          <td><a href="{{s.website}}">link</a></td>
          <td><a href="{{s.rss.twitter.url}}">link</a></td>
          <td><a href="{{s.rss.facebook.url}}">link</a></td>
        </tr>
        {% endfor %}
      </tbody>
    </table>
  </div>
</section>
<section id="university">
  <div class="container content">
    <h2 class="is-2">Universidades</h2>
    <table class="table is-striped is-bordered is-fullwidth">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Website</th>
          <th>Twitter</th>
          <th>Facebook</th>
        </tr>
      </thead>
      <tbody>
        {% for u in site.data.universities %}
        <tr>
          <td>{{u.name}}</td>
          <td>{{u.description}}</td>
          <td><a href="{{u.website}}">link</a></td>
          <td><a href="{{u.rss.twitter.url}}">link</a></td>
          <td><a href="{{u.rss.facebook.url}}">link</a></td>
        </tr>
        {% endfor %}
      </tbody>
    </table>
  </div>
</section>
