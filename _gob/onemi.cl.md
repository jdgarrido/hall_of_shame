---
layout: webshame
website: https://onemi.cl
name: ONEMI
description: Oficina Nacional de Emergencias
rss:
  twitter:
    url: https://twitter.com/onemichile
  facebook:
    url: https://www.facebook.com/onemi.cl
  phone:
    number: '600 586 7700'
---
