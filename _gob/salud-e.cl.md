---
layout: webshame
website: https://www.salud-e.cl
name: Estrategia e-Salud MinSal
description: Estrategia Digital en Salud o Plan de Salud-e
rss:
  twitter:
    url: https://twitter.com/salud_digital
  facebook:
    url:
  phone:
    number: '600 360 7777'
---
