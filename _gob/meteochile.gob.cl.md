---
layout: webshame
website: https://www.meteochile.gob.cl
name: MeteoChile
description: DIreccion Meteorologica de Chile
rss:
  twitter:
    url: http://twitter.com/meteochile_dmc
  facebook:
    url: https://www.facebook.com/meteochiledmc
  phone:
    number: '+56 2 2436 4538'
---
