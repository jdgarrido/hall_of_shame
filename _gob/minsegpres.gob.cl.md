---
layout: webshame
website: https://www.minsegpres.gob.cl
name: Ministerio Secretaría General de la Presidencia
description: Web Ministerio Secretaría General de la Presidencia
rss:
  twitter:
    url: https://twitter.com/segpres
  facebook:
    url:
  phone:
    number: '+56 2 2694 5888'
---

